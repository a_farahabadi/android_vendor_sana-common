LOCAL_PATH:= $(call my-dir)

RELATIVE_PACKAGEINSTALLER_PATH := ../../../packages/apps/PackageInstaller

include $(CLEAR_VARS)
include $(LOCAL_PATH)/../definitions.mk

LOCAL_USE_AAPT2 := true

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
    $(call all-java-files-under, src) \
    $(call all-java-files-under-except, $(RELATIVE_PACKAGEINSTALLER_PATH)/src, $(call all-java-files-under, src))

LOCAL_STATIC_ANDROID_LIBRARIES += \
    android-support-car \
    android-support-transition \
    android-support-compat \
    android-support-media-compat \
    android-support-core-utils \
    android-support-core-ui \
    android-support-fragment \
    android-support-v7-appcompat \
    android-support-v7-preference \
    android-support-v7-recyclerview \
    android-support-v14-preference \
    android-support-v17-leanback \
    android-support-v17-preference-leanback \
    car-list \
    SettingsLib

LOCAL_STATIC_JAVA_LIBRARIES := \
    xz-java \
    android-support-annotations

LOCAL_PACKAGE_NAME := SanaPackageInstaller
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := PackageInstaller

LOCAL_MANIFEST_FILE += $(LOCAL_PATH)/$(RELATIVE_PACKAGEINSTALLER_PATH)/AndroidManifest.xml

LOCAL_PROGUARD_FLAG_FILES := $(RELATIVE_PACKAGEINSTALLER_PATH)/proguard.flags

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/$(RELATIVE_PACKAGEINSTALLER_PATH)/res

# Comment for now unitl all private API dependencies are removed
# LOCAL_SDK_VERSION := system_current
LOCAL_PRIVATE_PLATFORM_APIS := true

include $(BUILD_PACKAGE)
