SANA_SOURCES := $(wildcard vendor/sana/*)
$(foreach s,$(SANA_SOURCES), \
    $(eval include $(s)/types.mk) \
)

# Set SANA_BUILDTYPE from the env, for jenkins compat
# Filter out random sana types, so it'll reset to DEV
ifeq ($(filter $(SANA_BUILDTYPES),$(SANA_BUILDTYPE)),)
    SANA_BUILDTYPE := DEV
endif
SANA_MAJOR_BUILDTYPE := $(shell echo $(SANA_BUILDTYPE) | cut -f 1 -d "_")
SANA_MINOR_BUILDTYPE := $(shell echo $(SANA_BUILDTYPE) | cut -f 2 -d "_")

SANA_MAJOR_BUILDTYPE_LOWER := $(shell echo $(SANA_MAJOR_BUILDTYPE) | tr A-Z a-z)
$(call inherit-product-if-exists, vendor/sana/$(SANA_MAJOR_BUILDTYPE_LOWER)/product.mk)
