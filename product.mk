# Project inherits
include $(LOCAL_PATH)/variants.mk
$(call inherit-product-if-exists, $(LOCAL_PATH)/prebuilt/common.mk)
$(call inherit-product-if-exists, $(LOCAL_PATH)/rootdir/common.mk)

# Overlay
PRODUCT_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(LOCAL_PATH)/overlay

# Security levels
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.sana.build.seclevel=$(SANA_BUILDTYPE) \
    ro.sana.build.seclevel.major=$(SANA_MAJOR_BUILDTYPE) \
    ro.sana.build.seclevel.minor=$(SANA_MINOR_BUILDTYPE)

# F-Droid Extension
PRODUCT_PACKAGES += \
    F-DroidPrivilegedExtension

# Custom sana Packages
PRODUCT_PACKAGES += \
    SanaSettings \
    SanaPackageInstaller

# Custom bootanimation
PRODUCT_PACKAGES += \
    bootanimation.zip_$(SANA_MAJOR_BUILDTYPE) \
    bootanimation.zip_$(SANA_BUILDTYPE)
