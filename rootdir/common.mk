# Sana init shell script
PRODUCT_PACKAGES += \
    init.sana.rc \
    init.sana.iptables.rc \
    init-sana.sh \
    init-sana.iptables.sh

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/etc/empty:$(TARGET_COPY_OUT_SYSTEM)/etc/empty
