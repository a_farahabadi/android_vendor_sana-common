# Default Apps
PRODUCT_PACKAGES += \
    MuPDF \
    MusicPlayer \
    PersianCalendar \

# priv-apps permissions
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/permissions/privapp-permissions-sana.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/privapp-permissions-sana.xml
