#!/system/bin/sh

# Clear iptables rules
#iptables -F
#iptables -X

# Set iptables rules
iptables_dropall="$(getprop ro.sana.iptables.dropall)"
iptables_whitelist_path="/data/misc/sana/iptables_whitelist"
iptables_blacklist_path="/data/misc/sana/iptables_blacklist"

if [ ! "$1" == "justdata" ]; then
if [ "$iptables_dropall" = "true" ]; then
    iptables -A INPUT -j DROP
    iptables -A OUTPUT -j DROP
fi

for num in $(seq 1 20)
do
    iptables_whitelist="$(getprop ro.sana.iptables.whitelist$num)"
    if [ ! -z "$iptables_whitelist" ]; then
        iptables -I INPUT -s "$iptables_whitelist" -j ACCEPT
        iptables -I OUTPUT -d "$iptables_whitelist" -j ACCEPT
    fi
done

for num in $(seq 1 20)
do
    iptables_blacklist="$(getprop ro.sana.iptables.blacklist$num)"
    if [ ! -z "$iptables_blacklist" ]; then
        iptables -I INPUT -s "$iptables_blacklist" -j DROP
        iptables -I OUTPUT -d "$iptables_blacklist" -j DROP
    fi
done
fi

if [ -f "$iptables_whitelist_path" ]; then
while read -r line
do
    iptables -I INPUT -s "$line" -j ACCEPT
    iptables -I OUTPUT -d "$line" -j ACCEPT
done < "$iptables_whitelist_path"
fi

if [ -f "$iptables_blacklist_path" ]; then
while read -r line
do
    iptables -I INPUT -s "$line" -j DROP
    iptables -I OUTPUT -d "$line" -j DROP
done < "$iptables_blacklist_path"
fi
