LOCAL_PATH := $(call my-dir)

RELATIVE_SETTINGS_PATH := ../../../packages/apps/Settings

include $(CLEAR_VARS)
include $(LOCAL_PATH)/../definitions.mk

LOCAL_PACKAGE_NAME := SanaSettings
LOCAL_PRIVATE_PLATFORM_APIS := true
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_USE_AAPT2 := true
LOCAL_OVERRIDES_PACKAGES := Settings

LOCAL_SRC_FILES := \
    $(call all-java-files-under, src) \
    $(call all-java-files-under-except, $(RELATIVE_SETTINGS_PATH)/src, $(call all-java-files-under, src))

LOCAL_STATIC_ANDROID_LIBRARIES := \
    android-slices-builders \
    android-slices-core \
    android-slices-view \
    android-support-compat \
    android-support-v4 \
    android-support-v13 \
    android-support-v7-appcompat \
    android-support-v7-cardview \
    android-support-v7-preference \
    android-support-v7-recyclerview \
    android-support-v14-preference

LOCAL_JAVA_LIBRARIES := \
    bouncycastle \
    telephony-common \
    ims-common

LOCAL_STATIC_JAVA_LIBRARIES := \
    android-arch-lifecycle-runtime \
    android-arch-lifecycle-extensions \
    guava \
    jsr305 \
    settings-logtags \
    org.lineageos.platform.internal

LOCAL_MANIFEST_FILE += $(LOCAL_PATH)/$(RELATIVE_SETTINGS_PATH)/AndroidManifest.xml

LOCAL_PROGUARD_FLAG_FILES := $(RELATIVE_SETTINGS_PATH)/proguard.flags

ifneq ($(INCREMENTAL_BUILDS),)
    LOCAL_PROGUARD_ENABLED := disabled
    LOCAL_JACK_ENABLED := incremental
    LOCAL_JACK_FLAGS := --multi-dex native
endif

LOCAL_RESOURCE_DIR := \
    $(LOCAL_PATH)/res \
    $(LOCAL_PATH)/$(RELATIVE_SETTINGS_PATH)/res

include frameworks/opt/setupwizard/library/common-gingerbread.mk
include frameworks/base/packages/SettingsLib/common.mk

include $(BUILD_PACKAGE)
