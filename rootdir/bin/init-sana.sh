#!/system/bin/sh

major_seclevel="$(getprop ro.sana.build.seclevel.major | tr A-Z a-z)"
/system/bin/init-sana.${major_seclevel}.sh || true

disable_camera="$(getprop ro.sana.camera.disabled)"
disable_wifi="$(getprop ro.sana.wifi.disabled)"
disable_bt="$(getprop ro.sana.bt.disabled)"
disable_nfc="$(getprop ro.sana.nfc.disabled)"
disable_usb="$(getprop ro.sana.usb.disabled)"
disable_gps="$(getprop ro.sana.gps.disabled)"

# Setup sana tmpdir
mkdir -p /mnt/sana/
mount -t tmpfs -o rw,nodev,relatime,mode=755,gid=0 none /mnt/sana || true
mkdir /mnt/sana/empty_dir

# Copy manifest
manifest="/vendor/etc/vintf/manifest.xml"
mnai_ctxt="$(ls -lZ $manifest | grep -oE 'u:object_r:[^:]*:s0')"
mani_copy="/mnt/sana/$(echo $manifest | tr / _)"
cp -a $manifest $mani_copy

# Disable Camera
if [ "$disable_camera" = "true" ]; then
    if grep android.hardware.camera.provider $manifest; then
        sed -i "s|android.hardware.camera.provider|android.hardware.camera.provider.disabled|g" $mani_copy
    fi
fi

# Disable WIFI
if [ "$disable_wifi" = "true" ]; then
    if grep android.hardware.wifi $manifest; then
        sed -i "s|android.hardware.wifi|android.hardware.wifi.disabled|g" $mani_copy
        sed -i "s|android.hardware.wifi.disabled.hostapd|android.hardware.wifi.hostapd|g" $mani_copy
    fi
fi

# Disable Bluetooth
if [ "$disable_bt" = "true" ]; then
    if grep android.hardware.bluetooth $manifest; then
        sed -i "s|android.hardware.bluetooth|android.hardware.bluetooth.disabled|g" $mani_copy
        mount -o bind /mnt/sana/empty_dir /system/app/Bluetooth || true
        mount -o bind /mnt/sana/empty_dir /system/app/BluetoothMidiService || true
    fi
fi

# Disable NFC
if [ "$disable_nfc" = "true" ]; then
    if grep android.hardware.nfc $manifest; then
        mount -o bind /system/etc/empty /system/etc/permissions/android.hardware.nfc.xml || true
        mount -o bind /system/etc/empty /system/etc/permissions/android.hardware.nfc.hce.xml || true
        mount -o bind /system/etc/empty /vendor/etc/permissions/android.hardware.nfc.xml || true
        mount -o bind /system/etc/empty /vendor/etc/permissions/android.hardware.nfc.hce.xml || true
    fi
fi

# Disable GPS
if [ "$disable_gps" = "true" ]; then
    mount -o bind /system/etc/empty /system/etc/permissions/android.hardware.location.gps.xml || true
fi


# Apply manifest changes
chcon $mnai_ctxt $mani_copy
mount -o bind $mani_copy $manifest || true
