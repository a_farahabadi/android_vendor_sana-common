###########################################################
# Use utility find to find given files in the given subdirs.
# This function uses $(1), instead of LOCAL_PATH as the base.
# $(1): the base dir, relative to the root of the source tree.
# $(2): the file name pattern to be passed to find as "-name".
# $(3): a list of subdirs of the base dir.
# $(4): a list of files paths to exclude.
# Returns: a list of paths relative to the base dir.
###########################################################

define find-files-in-subdirs-except
$(sort $(patsubst ./%,%, \
  $(shell cd $(1) ; \
          find -L $(3) -name $(2) -and -not -name ".*" | grep -vE `echo $(4) | tr ' ' '|'`) \
 ))
endef

###########################################################
## Find all of the files under the named directories with
## the specified name with exceptions.
## Meant to be used like:
##    SRC_FILES := $(call all-named-files-under,*.h,src tests,except)
###########################################################

define all-named-files-under-except
$(call find-files-in-subdirs-except,$(LOCAL_PATH),"$(1)",$(2),$(3))
endef

###########################################################
## Find all of the java files under the named directories 
## with exceptions.
## Meant to be used like:
##    SRC_FILES := $(call all-java-files-under,src tests,except)
###########################################################

define all-java-files-under-except
$(call all-named-files-under-except,*.java,$(1),$(2))
endef

